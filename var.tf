variable "cdir" {
  description = "here we describe"
  default     = "10.0.0.0/16"
}
variable "InstanceType" {
  description = "here we describe"
  default     = "InstanceType"
}
variable "AWS_REGION" {
  description = "AWS region"
  default     = "eu-west-1"
}
variable "private_subnet_CIDR" {
  description = "here we describe"
  type        = list(string)
  default     = ["10.0.1.0/24"]
}
variable "public_subnet_CIDR" {
  description = "here we describe"
  type        = list(string)
  default     = ["10.0.101.0/24"]
}
variable "azs" {
  description = "here we describe"
  type        = list(string)
  default     = ["eu-west-1a", "eu-west-1b"]
}
variable "key" {
  description = "here we describe"
  default     = "Jenkins_key"
}
variable "ami" {
  description = "here we describe"
  default     = "ami-02b4e72b17337d6c1"
}