/*==== The VPC ======*/
# Create a custom VPC with 3 private subnets and 3 public subnets
module "vpc" {
  source               = "terraform-aws-modules/vpc/aws"
  cidr                 = var.cdir
  name                 = "Tera-VPC"
  private_subnets      = var.private_subnet_CIDR
  public_subnets       = var.public_subnet_CIDR
  azs                  = var.azs
  create_igw           = true
  enable_nat_gateway   = true
  enable_vpn_gateway   = false
  enable_dns_hostnames = true
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
resource "aws_elb" "jenkins" {
  count    = 1
  name     = "jenkins-lb"
  internal = false
  subnets  = module.vpc.public_subnets.*
  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  tags = {
    Environment = "production"
  }
}
resource "aws_security_group" "bastion accsess" {
  name        = "bastion accsess"
  description = "Allow ssh inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress = [
    {
      description      = "ssh from VPC"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  ]

  egress = [
    {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  ]

  tags = {
    Name = "allow_tls"
  }
}
resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress = [
    {
      description      = "TLS from VPC"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = [aws_vpc.main.cidr_block]
      ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
    }
  ]

  egress = [
    {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  ]

  tags = {
    Name = "allow_tls"
  }
}