module "bastion" {
  source           = "umotif-public/bastion/aws"
  version          = "~> 2.1.0"
  name_prefix      = "Tera"
  vpc_id           = module.vpc.vpc_id
  public_subnets   = flatten([module.vpc.public_subnets])
  min_size         = 1
  max_size         = 1
  desired_capacity = 1
  hosted_zone_id   = ""
  ssh_key_name     = var.key
  tags = {
    Name    = "BastionTheKIng"
    Project = "Test"
  }
}
module "ec2_instance" {
  source        = "terraform-aws-modules/ec2-instance/aws"
  version       = "~> 3.0"
  name          = "Jenkins-instance"
  count         = 1
  ami           = var.ami
  instance_type = var.InstanceType
  key_name      = var.key
  monitoring    = false
  subnet_id     = element(module.vpc.private_subnets.*, count.index)
  tags = {
    Tier        = "Private"
    Terraform   = "true"
    Environment = "dev"
  }
}